const AWS = require('aws-sdk');
const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
const util = require("util");
const request = require("request");
const requestPromise = util.promisify(request);

exports.handler = async (event) => {
    const params = {
        Bucket: 'intel-files-system'
    }

    let s3Objects;

    try {
        s3Objects = await s3.listObjectsV2(params).promise();

        const objectsLength = s3Objects.Contents.length;
        if (objectsLength % 7 === 0) {
            const reqOptions = {
                method: "POST",
                headers: {},
                uri: "http://ec2-18-185-139-151.eu-central-1.compute.amazonaws.com:8082/fileAssignments/start"
            }
            const responseData = await requestPromise(reqOptions);
            console.log(`Request sent to post-processing server..`);
            return { statusCode: responseData.statusCode };
        }
    } catch (e) {
        console.log(e);
        const response = {
            statusCode: 400,
            body: JSON.stringify(e),
        };
        return response;
    }
};
